
3D Shape Generator Created With Unity Game Studio.

* The user have a set of geocentric figures to choose from add a color or save the shape on the plain in the background
* Project is created with Unity 5.4.2f2, C#
* Project can be built for multiple platforms
* Date, 7/11/2016


### Guidelines ###

* Three on Screen Buttons
  * Shape: Changes The 3D Shape
  * Color: Changes The color of the Shape
  * Save: Transforms a copy of the shape picked to the background plain

### Screenshots ###

![3DShapeGenerator_11.jpg](https://bitbucket.org/repo/x4XaXe/images/1837106340-3DShapeGenerator_11.jpg)