﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeControl : MonoBehaviour
{

    GameObject cube;
    GameObject cone;
    GameObject sphere;
    GameObject clone;
    int activeObjectIndex;
    GameObject activeGameObject;
    int activeColorIndex;
    Color selectedColor;
    float cloneSelectedX;
    float cloneSelectedZ;
    int changeShapeFlag;
    int cloneFlag;
    int changeColorFlag;

    void Start()
    {
        cube = GameObject.Find("Shape/Cube");
        PutColor(cube);
        cone = GameObject.Find("Shape/Cone");
        PutColor(cone);
        sphere = GameObject.Find("Shape/Sphere");
        PutColor(sphere);
        activeGameObject = cube;
        cone.SetActive(false);
        sphere.SetActive(false);
        cloneSelectedX = -4;
        cloneSelectedZ = 12;
    }

    void PutColor(GameObject activeshape)
    {
        activeshape.GetComponent<Renderer>().material.color = selectedColor;
    }
    void SetShape()
    {
        cone.SetActive(false);
        cube.SetActive(false);
        sphere.SetActive(false);
        activeGameObject.SetActive(true);
    }
    void SelectActiveGameObject()
    {
        activeObjectIndex++;
        if (activeObjectIndex > 2) activeObjectIndex = 0;
        if (activeObjectIndex == 0) activeGameObject = cube;
        if (activeObjectIndex == 1) activeGameObject = cone;
        if (activeObjectIndex == 2) activeGameObject = sphere;

    }
    void SelectActiveGameColor()
    {
        activeColorIndex++;
        if (activeColorIndex > 1) activeColorIndex = 0;
        if (activeColorIndex == 0) selectedColor = Color.red;
        if (activeColorIndex == 1) selectedColor = Color.black;

    }
    void CloneShapeSelected()
    {
        clone = Instantiate(activeGameObject);
        PutColor(clone);
        clone.transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f);
        PosClone();

    }
    void PosClone()
    {
        if (cloneSelectedX > 4)
        {
            cloneSelectedZ -= 1f;
            cloneSelectedX = -4;
        }
        clone.transform.position = new Vector3(cloneSelectedX, 0.5F, cloneSelectedZ);
        cloneSelectedX += 0.9f;

    }

    void OnGUI()
    { 
        if (GUI.Button(new Rect(Screen.width / 2-250, 10, 150, 50), "Shape"))
            changeShapeFlag = 1;
        if (GUI.Button(new Rect(Screen.width / 2-50, 10, 150, 50), "Save"))
            cloneFlag = 1;
        if (GUI.Button(new Rect(Screen.width / 2+150, 10, 150, 50), "Color"))
            changeColorFlag = 1;
    }

    void Update()
    {

        if (changeShapeFlag == 1)
        {
            SelectActiveGameObject();
            SetShape();
            changeShapeFlag = 0;
        }

        if (changeColorFlag == 1)
        {
            SelectActiveGameColor();
            PutColor(activeGameObject);
            changeColorFlag = 0;
        }
        if (cloneFlag == 1)
        {
            CloneShapeSelected();
            cloneFlag = 0;
        }
    }
}

